# Spruce GQL 2


### Live deployment

```
https://d9au7i360a.execute-api.us-east-1.amazonaws.com/production/graphql
```

To test with Insomnia, import the `Insomnia_graphql.yaml` collection into the API testing tool [Insomnia](https://insomnia.rest/).


### Quickstart

The easiest way to run this application locally is with docker.  Make sure that you have docker and docker-compose installed on your machine.

Run:

```
docker-compose up -d
```

and wait about 20-30 seconds for the databases to get initialized.  This compose file sets up instances of the:

- api
- legacy db (mysql)
- modern db (postgres)

The local databases are initialized with similar data to what is in the AWS production databases.  Testing in this docker configuration gives us a "playground" to use without affecting the production data.

To access the GraphQL API, navigate to:

```
http://localhost:5000/graphql
```

This interface can be used to test the API.  Alternatively, you can import the `Insomnia_graphql.yaml` collection into the API testing tool [Insomnia](https://insomnia.rest/).


### Local development

For development, its often more convenient to run on your local machine instead of docker.  To do this, you need to create a python virtual environment and install the requirements.  First, make sure you have [pipenv installed](https://pipenv.pypa.io/en/latest/#install-pipenv-today).  Then:


```
pipenv shell
pipenv install --skip-lock
export FLASK_APP=main.py
flask run
```

If you want to run python locally, just make sure that you do `docker-compose stop api` beforehand so you don't have a port conflict.

### Deployment

The application is automatically deployed using Gitlab CI/CD on every `git push`, as long as it passes the test first.  You can modify the CI/CD config by modifying the **.gitlab-ci.yml** file.

The live URL is:

```
https://d9au7i360a.execute-api.us-east-1.amazonaws.com/production/graphql
```

To manually deploy the application, follow the above steps under **Local development**, and then run:

```
zappa update production
```


### Environment variables
There are 2 env vars used, `SPRUCE_LEGACY_DB`, and `SPRUCE_MODERN_DB`.  Setting these tells the application which database to connect to.  For docker, it will automatically use local databases, and in production it uses the AWS databases.

If you wish to use the production databases in local testing, you can export these variables into your environment to override the defaults, or create a .env file which Flask will pick up automatically.




### Testing

Tests are written using `pytest`.  The tests are run automatically in Gitlab CI/CD upon every commit.  They can also be run locally in your docker or python environment.  If your docker containers are running, run:

```
docker-compose exec api bash run_tests.sh
```

To run the tests.  You should see an output similar to below:

```
tests/test_api.py::test_list_properties[Valid - list properties] PASSED                                                [ 16%]
tests/test_api.py::test_get_property[Valid - get property] PASSED                                                      [ 33%]
tests/test_api.py::test_update_property[Valid - list properties] PASSED                                                [ 50%]
tests/test_api.py::test_create_property[Valid - list properties] PASSED                                                [ 66%]
tests/test_api.py::test_legacy_property_search[Valid - list properties] PASSED                                         [ 83%]
tests/test_api.py::test_modern_property_search[Valid - list properties] PASSED                                         [100%]
```


### Seeding the database

You can seed the local docker databases with sample data by running:

```
python seed_databases.py
```

in your python environment.  It will add 100 fake entries to both the databases.
