--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: properties; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.properties (
    id text DEFAULT ''::text NOT NULL,
    name text NOT NULL,
    legacy_id text NOT NULL,
    is_live boolean NOT NULL,
    go_live_date date NOT NULL,
    numer_of_units numeric NOT NULL
);


ALTER TABLE public.properties OWNER TO postgres;

--
-- Name: properties_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.properties_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.properties_id_seq OWNER TO postgres;

--
-- Name: properties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.properties_id_seq OWNED BY public.properties.id;


--
-- Data for Name: properties; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.properties (id, name, legacy_id, is_live, go_live_date, numer_of_units) FROM stdin;
e6418d07-a7e1-4095-a79e-b12a3bc3dc15	2040 on 6th	2	t	2021-02-01	120
cdd3fb9c-0535-4856-afd4-6b145536c6ea	The Estates	4	f	2021-04-09	104
89920f68-6b61-4fea-91ac-00c056550015	Riverside Commons	3	t	2020-07-23	102
a0f13a08-f6cf-4f15-a32c-619e5a8df027	Millenium	5	t	2021-01-02	80
36c79ea5-41b1-41b9-bdd5-6b6d6708e823	The Falls at Westgate	1	f	2021-05-01	216
c6ac5e0e-bb2e-4f6c-aec7-590561820ae1	Corazon1	23	f	2021-10-10	55
\.


--
-- Name: properties_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.properties_id_seq', 5, true);


--
-- Name: properties properties_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.properties
    ADD CONSTRAINT properties_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

