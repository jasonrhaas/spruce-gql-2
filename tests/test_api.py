import pytest


@pytest.mark.parametrize('params, expected_code, expected_key', [
    pytest.param(
        """
        query {
          listProperties {
            success
            errors
            properties {
              id
              name
              address
              city
              state
              zip
              is_live
              go_live_date
              number_of_units
            }
          }
        }""",
        200,
        'r.json["data"["listProperties"]["properties"]',
        id='Valid - list properties'
    ),
])
def test_list_properties(client, params, expected_code, expected_key):
    r = client.get('/graphql', query_string=params)

    assert r.status_code == expected_code
    assert expected_key is not None


@pytest.mark.parametrize('params, expected_code, expected_key', [
    pytest.param(
        """
        query {
          getProperty(propertyId: "1") {
            success
            errors
            property {
              id
              name
              address
              city
              state
              zip
              is_live
              go_live_date
              number_of_units
            }
          }
        }""",
        200,
        'r.json["data"["getProperty"]["property"]',
        id='Valid - get property'
    ),
])
def test_get_property(client, params, expected_code, expected_key):
    r = client.get('/graphql', query_string=params)

    assert r.status_code == expected_code
    assert expected_key is not None


@pytest.mark.parametrize('params, expected_code, expected_key', [
    pytest.param(
        """
        mutation updateProperty {
          updateProperty(propertyId: 17, number_of_units: 101, zip: "123456") {
            success
            errors
            property {
            id
            name
            }
          }
        }""",
        200,
        'r.json["data"["updateProperty"]["property"]',
        id='Valid - list properties'
    ),
])
def test_update_property(client, params, expected_code, expected_key):
    r = client.get('/graphql', query_string=params)

    assert r.status_code == expected_code
    assert expected_key is not None


@pytest.mark.parametrize('params, expected_code, expected_key', [
    pytest.param(
        """
        mutation createProperty {
          createProperty(name: "Test Entry", address: "East Side", city: "Austin", state: "TX", zip: "78702", is_live: false, go_live_date: "2021-10-10", number_of_units: 55) {
            success
            errors
            property {
            id
            name
            }
          }
        }""",
        200,
        'r.json["data"["createProperty"]["property"]',
        id='Valid - list properties'
    ),
])
def test_create_property(client, params, expected_code, expected_key):
    r = client.get('/graphql', query_string=params)

    assert r.status_code == expected_code
    assert expected_key is not None


@pytest.mark.parametrize('params, expected_code, expected_key', [
    pytest.param(
        """
        query {
          legacyPropertySearch(propertyName: "falls"){
            success
            errors
            legacyProperties {
              id
              name
              address
              city
              state
              zip
            }
          }
        }""",
        200,
        'r.json["data"["legacyPropertySearch"]["legacyProperties"]',
        id='Valid - list properties'
    ),
])
def test_legacy_property_search(client, params, expected_code, expected_key):
    r = client.get('/graphql', query_string=params)

    assert r.status_code == expected_code
    assert expected_key is not None


@pytest.mark.parametrize('params, expected_code, expected_key', [
    pytest.param(
        """
        query modernPropertySearch{
          modernPropertySearch(propertyName: "com"){
            success
            errors
            modernProperties {
              id
              name
            }
          }
        }""",
        200,
        'r.json["data"["modernPropertySearch"]["modernProperties"]',
        id='Valid - list properties'
    ),
])
def test_modern_property_search(client, params, expected_code, expected_key):
    r = client.get('/graphql', query_string=params)

    assert r.status_code == expected_code
    assert expected_key is not None
