import logging

import pytest
from api import api

logging.basicConfig(level=logging.CRITICAL)


@pytest.fixture(scope='session')
def client(pytestconfig):
    """
        Inject flask config values based on the stage.
    """

    test_config = dict(
        SQLALCHEMY_DATABASE_URI='mysql+pymysql://admin:spruce@mysql:3306/legacy-db',

        SQLALCHEMY_BINDS={
            'legacy': 'mysql+pymysql://admin:spruce@mysql:3306/legacy-db',
            'modern': 'postgresql://postgres:spruce@postgres:5432/modern-db'
            # 'legacy': 'mysql+pymysql://admin:spruce@localhost:3306/legacy-db',
            # 'modern': 'postgresql://postgres:spruce@localhost:5432/modern-db'
        }
    )
    api.init_app(**test_config)
    app = api.app
    client = app.test_client()

    return client
