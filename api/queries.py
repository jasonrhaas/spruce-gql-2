from datetime import datetime

from api import api
# from api.models import LegacyProperty, ModernProperty
from api.utils import combine_properties
from ariadne import convert_kwargs_to_snake_case
from flask import current_app

LegacyProperty = api.models['LegacyProperty']
ModernProperty = api.models['ModernProperty']
db = api.db
db_modern = api.db_modern


@convert_kwargs_to_snake_case
def resolve_properties(obj, info):
    """ Grabs all the properties from both the legacy and modern database and combines them.
        If there is overlap, the values in the legacy database take priority.
    """

    try:
        legacy_properties = [property_.to_dict() for property_ in LegacyProperty.query.all()]
        modern_properties = dict()
        for modern_property in ModernProperty.query.all():
            # Make modern_properties a dictionary for fast lookups and easy joins
            modern_properties[modern_property.to_dict()['legacy_id']] = modern_property.to_dict()

        # Logic to combine properties from both dbs
        properties = []
        for legacy_property in legacy_properties:
            # TODO: Make sure to handle edge cases here.
            if str(legacy_property['id']) in modern_properties:
                property_ = {**modern_properties[str(legacy_property['id'])], **legacy_property}
            else:
                current_app.logger.debug(f'legacy_id: {legacy_property["id"]}')
                current_app.logger.warning('Mismatch between legacy and modern dbs')
                continue

            # Convert Python types to formats compatible with JSON
            property_['go_live_date'] = property_['go_live_date'].strftime('%Y-%m-%d')
            property_['number_of_units'] = int(property_['number_of_units'])
            current_app.logger.info(property_)

            properties.append(property_)

        payload = {
            "success": True,
            "properties": properties
        }
    except Exception as error:
        current_app.logger.error(error)
        payload = {
            "success": False,
            "errors": [str(error)]
        }
    return payload


@convert_kwargs_to_snake_case
def resolve_legacy_property_search(obj, info, property_name):
    """ Do a basic 'contains' search on the property name.
        Search is case insenstive and will return a result if the text matches anywhere in the property_name.

        Note:  The snake_case decoraor applies to all inputs and outputs of this function.
               Instead of returning "legacyProperties", need to return "legacy_properties"

    """
    legacy_properties = [property_.to_dict() for property_ in LegacyProperty.query.filter(LegacyProperty.name.ilike(f'%{property_name}%')).all()]
    current_app.logger.info(legacy_properties)

    payload = {
        "success": True,
        "legacy_properties": legacy_properties
    }
    current_app.logger.info(payload)
    print(payload)

    return payload


@convert_kwargs_to_snake_case
def resolve_modern_property_search(obj, info, property_name):
    """ Do a basic 'contains' search on the property name.
        Search is case insenstive and will return a result if the text matches anywhere in the property_name.

        Note:  The snake_case decoraor applies to all inputs and outputs of this function.
               Instead of returning "legacyProperties", need to return "legacy_properties"
    """
    modern_properties = [property_.to_dict() for property_ in ModernProperty.query.filter(ModernProperty.name.ilike(f'%{property_name}%')).all()]
    current_app.logger.info(modern_properties)

    payload = {
        "success": True,
        "modern_properties": modern_properties
    }

    return payload


@convert_kwargs_to_snake_case
def resolve_property(obj, info, property_id):
    """
    Lookup a property by its legacy_id, and combine fields as neccessary.
    """
    try:
        legacy_property = LegacyProperty.query.get(property_id)
        modern_property = ModernProperty.query.filter_by(legacy_id=property_id).first()
        if not legacy_property or not modern_property:
            payload = {
                "success": False,
                "errors": ["Property ID not found."]
            }
            return payload
        property_ = combine_properties(legacy_property.to_dict(), modern_property.to_dict())
        current_app.logger.info(property_)

        payload = {
            "success": True,
            "property": property_
        }
    except Exception as error:
        current_app.logger.error(error)
        payload = {
            "success": False,
            "errors": [str(error)]
        }
    return payload
