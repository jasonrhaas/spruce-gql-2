
import uuid
from datetime import datetime

from api import api
from api.utils import combine_properties
from ariadne import convert_kwargs_to_snake_case
from flask import current_app

LegacyProperty = api.models['LegacyProperty']
ModernProperty = api.models['ModernProperty']
db = api.db
db_modern = api.db_modern


@convert_kwargs_to_snake_case
def resolve_create_property(obj, info, name, address, city, state, zip, is_live, go_live_date, number_of_units):
    try:
        legacy_property = LegacyProperty(
            name=name,
            address=address,
            city=city,
            state=state,
            zip=zip
        )
        db.session.add(legacy_property)
        db.session.commit()

        # Convert date string to a date object
        go_live_date = datetime.strptime(go_live_date, '%Y-%m-%d').date()
        modern_property = ModernProperty(
            id=str(uuid.uuid4()),
            name=name,
            legacy_id=legacy_property.id,
            is_live=is_live,
            go_live_date=go_live_date,
            numer_of_units=number_of_units
        )
        db_modern.session.add(modern_property)
        db_modern.session.commit()

        new_legacy_property = LegacyProperty.query.get(legacy_property.id).to_dict()
        new_modern_property = ModernProperty.query.filter_by(legacy_id=str(legacy_property.id)).first().to_dict()
        property_ = combine_properties(new_legacy_property, new_modern_property)

        payload = {
            "success": True,
            "property": property_
        }
    except ValueError:
        payload = {
            "success": False,
            "errors": ["Something went wrong with property creation"]
        }
    return payload


@convert_kwargs_to_snake_case
def resolve_update_property(obj, info, property_id, **kwargs):
    try:
        legacy_property = LegacyProperty.query.get(property_id)

        legacy_args = ('name', 'address', 'city', 'state', 'zip')
        for k, v in kwargs.items():
            # Only update columns that are specified
            if k in legacy_args and v:
                setattr(legacy_property, k, v)

        current_app.logger.info(legacy_property)
        db.session.add(legacy_property)
        db.session.commit()

        modern_property = ModernProperty.query.filter_by(legacy_id=str(legacy_property.id)).first()

        modern_args = ('is_live', 'go_live_date', 'number_of_units')
        for k, v in kwargs.items():
            # Only update columns that are specified
            if k in modern_args and v:
                if k == 'go_live_date':
                    v = datetime.strptime(v, '%Y-%m-%d').date()
                # Workaround for typo in database
                if k == 'number_of_units':
                    k = 'numer_of_units'
                setattr(modern_property, k, v)

        current_app.logger.info(modern_property)
        db_modern.session.add(modern_property)
        db_modern.session.commit()

        new_legacy_property = LegacyProperty.query.get(legacy_property.id).to_dict()
        new_modern_property = ModernProperty.query.filter_by(legacy_id=str(legacy_property.id)).first().to_dict()
        property_ = combine_properties(new_legacy_property, new_modern_property)

        payload = {
            "success": True,
            "property": property_
        }
    except AttributeError:
        payload = {
            "success": False,
            "errors": [f"Property matching id {property_id} was not found"]
        }

    return payload
