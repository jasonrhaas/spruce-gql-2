from flask import current_app


def combine_properties(legacy_property: dict, modern_property: dict):
    # TODO:  Make sure to handle edge cases here.  What if the property_id is not found in either legacy or modern db?
    property_ = {**modern_property, **legacy_property}

    # Convert Python types to formats compatible with JSON
    property_['go_live_date'] = property_['go_live_date'].strftime('%Y-%m-%d')
    property_['number_of_units'] = int(property_['number_of_units'])

    return property_
