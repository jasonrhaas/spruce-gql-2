
from api.config import Config
from api.models import load_models
from ariadne import (ObjectType, graphql_sync, load_schema_from_path,
                     make_executable_schema, snake_case_fallback_resolvers)
from ariadne.constants import PLAYGROUND_HTML
from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy


class Api:

    def init_app(self, **extra_configs):
        app = Flask(__name__)

        # Import config object
        app.config.from_object(Config)
        # Inject new config values or override existing ones
        app.config = {**app.config, **extra_configs}

        # Create database connections for legacy and modern db
        db = SQLAlchemy(app)
        db_modern = SQLAlchemy(app)

        # Reflect schemas from each database into our model
        db.reflect(bind='legacy')
        db_modern.reflect(bind='modern')

        # Load models
        self.models = load_models(db, db_modern)

        self.app = app
        self.db = db
        self.db_modern = db_modern
        self.extra_configs = extra_configs

        from api.mutations import resolve_create_property, resolve_update_property
        from api.queries import (resolve_legacy_property_search,
                                 resolve_modern_property_search, resolve_properties,
                                 resolve_property)

        query = ObjectType("Query")

        query.set_field("listProperties", resolve_properties)
        query.set_field("getProperty", resolve_property)
        query.set_field("legacyPropertySearch", resolve_legacy_property_search)
        query.set_field("modernPropertySearch", resolve_modern_property_search)

        mutation = ObjectType("Mutation")
        mutation.set_field("createProperty", resolve_create_property)
        mutation.set_field("updateProperty", resolve_update_property)

        type_defs = load_schema_from_path("schema.graphql")
        schema = make_executable_schema(
            type_defs,
            query,
            mutation,
            snake_case_fallback_resolvers
        )

        @app.route('/')
        def hello():
            return 'Hello!'

        @app.route("/graphql", methods=["GET"])
        def graphql_playground():
            return PLAYGROUND_HTML, 200

        @app.route("/graphql", methods=["POST"])
        def graphql_server():
            data = request.get_json()

            success, result = graphql_sync(
                schema,
                data,
                context_value=request,
                debug=app.debug
            )

            status_code = 200 if success else 400
            return jsonify(result), status_code
