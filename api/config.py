import os


class Config:
    """ These are the default config values for the flask application.  They can be changed depending on the need.

        You can set the environment variables in your local environment using `export`, for example `export USE_MARIADB=false`.
        Alternatively you can edit the .env file and run `pipenv shell` to activate it.

        For the pytest suite, these values are injected into the api.init_app() method.
    """

    # Flask built ins.  Only set these in environment, not in this class
    # FLASK_ENV
    # FLASK_APP
    # FLASK_DEBUG
    # SECRET_KEY

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SQLALCHEMY_DATABASE_URI = os.getenv('SPRUCE_LEGACY_DB', 'mysql+pymysql://admin:spruce@localhost:3306/legacy-db')
    SQLALCHEMY_BINDS = {
        'legacy': os.getenv('SPRUCE_LEGACY_DB', 'mysql+pymysql://admin:spruce@localhost:3306/legacy-db'),
        'modern': os.getenv('SPRUCE_MODERN_DB', 'postgresql://postgres:spruce@localhost:5432/modern-db')
    }


def api_response(code, **extras):
    """ Return a standardized error response, and error code.
        This function is designed to work with Connexion.
    """

    res = dict(
        message=code_lookup.get(code, 'An unknown error occured.')
    )

    if extras:
        res = {**res, **extras}

    if code == 200:
        res.pop('message')

    return res, code


code_lookup = {
    200: 'OK',
    201: 'Request successful',
    204: 'No content',
    400: 'Invalid input, object invalid',
    401: 'Unauthorized',
    403: 'Forbidden',
    404: 'Not found',
    405: 'Method not allowed',
    409: 'Data already exists',
    418: "I'm a teapot",
    429: "Too many requests",
    422: "Unprocessable Entity",
    500: 'A server error occured',
    501: 'Not implemented'
}
