
def load_models(db, db_modern):
    class LegacyProperty(db.Model):
        """ Model for legacy MySQL db """
        __bind_key__ = 'legacy'
        __table__ = db.Model.metadata.tables['properties']

        def to_dict(self):
            return {
                "id": self.id,
                "name": self.name,
                "address": self.address,
                "city": self.city,
                "state": self.state,
                "zip": self.zip
            }

    class ModernProperty(db_modern.Model):
        """ Model for modern Postgres db """
        __bind_key__ = 'modern'
        __table__ = db_modern.Model.metadata.tables['properties']

        def to_dict(self):
            return {
                "id": self.id,
                "name": self.name,
                "legacy_id": self.legacy_id,
                "is_live": self.is_live,
                "go_live_date": self.go_live_date,
                # There is a typo in the column name.  Fix this in the interface.
                "number_of_units": self.numer_of_units
            }

    return dict(LegacyProperty=LegacyProperty, ModernProperty=ModernProperty)
