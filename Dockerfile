FROM python:3.7.10-buster
MAINTAINER Jason Haas <jason@bazze.io>

# Track build number (for automated build systems)
ARG BUILD_NUMBER=0
ENV BUILD_NUMBER $BUILD_NUMBER

COPY requirements.txt .
RUN pip install -U pip
RUN pip install -r requirements.txt

# Set up code directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Copy over code
COPY main.py .
COPY schema.graphql .
COPY api api/
# COPY tests tests/
ENV FLASK_APP main.py

EXPOSE 5000
CMD ["flask", "run", "--host", "0.0.0.0"]

