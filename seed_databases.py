import random
import uuid

from api import api
from faker import Faker

fake = Faker()


api.init_app()
app = api.app

LegacyProperty = api.models['LegacyProperty']
ModernProperty = api.models['ModernProperty']
db = api.db
db_modern = api.db_modern

ids = list(range(100, 200))

for id_ in ids:
    is_live = [True, False]

    legacy_property = LegacyProperty(
        id=id_,
        name=fake.name(),
        address=fake.street_address(),
        city=fake.city(),
        state=fake.state(),
        zip=fake.zipcode()
    )

    db.session.add(legacy_property)
db.session.commit()

modern_ids = [str(uuid.uuid4()) for id_ in ids]

for id_, modern_id in zip(ids, modern_ids):
    is_live = [True, False]

    modern_property = ModernProperty(
        id=modern_id,
        name=fake.name(),
        legacy_id=id_,
        is_live=random.choice(is_live),
        go_live_date=fake.date(),
        numer_of_units=random.randint(10, 1000)
    )

    db_modern.session.add(modern_property)
db_modern.session.commit()
